var fs = require('fs');
var {addDays, format} = require('date-fns')

if (process.argv.length < 3) {
  console.error('error: missing argument: schema json file');
  process.exit(1)
}

let write = console.log

let schema = JSON.parse(fs.readFileSync(process.argv[2]))
let daysChars = schema.days.join('');
if (daysChars.length % 7 != 0) {
  console.error('error: days total character count must be multiple of 7');
  process.exit(1)
}
let weeksPerCycle = daysChars.length / 7
write(`BEGIN:VCALENDAR
PRODID:schema2ical.js generate cyclic calendar for CC
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALNAME:cc arbetstid
X-WR-TIMEZONE:Europe/Stockholm
BEGIN:VTIMEZONE
TZID:Europe/Stockholm
X-LIC-LOCATION:Europe/Stockholm
BEGIN:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
TZNAME:CEST
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
END:STANDARD
END:VTIMEZONE
`)
for (let i = 0; i < daysChars.length; i++) {
  let dayFromStart = schema['start-index'] + i
  let c = daysChars[dayFromStart % daysChars.length]
  if ('a' <= c && c <= 'z') {
    let dateFunc = j => format(
        addDays(new Date(`${schema['start-date']}T${schema.times[c][j]}:00`), i), "yyyyMMdd'T'HHmm'00'")
    write(`BEGIN:VEVENT
DTSTART;TZID=Europe/Stockholm:${dateFunc(0)}
DTEND;TZID=Europe/Stockholm:${dateFunc(1)}
RRULE:FREQ=WEEKLY;INTERVAL=${weeksPerCycle}
SUMMARY:cc jobbar
END:VEVENT`)
  }
}
write("END:VCALENDAR");
